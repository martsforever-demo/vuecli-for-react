const publicPath = '/vuecli-for-react/'

module.exports = {
    publicPath,
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "~plain-design/src/styles/global-import";`
            },
        }
    },
}