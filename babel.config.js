module.exports = {
    presets: [
        '@babel/preset-react'
    ],
    plugins: [
        [
            'import',
            {
                libraryName: "plain-design",
                camel2DashComponentName: false,
                customName: (name) => {
                    return `plain-design/src/packages/${name}/`;
                }
            }
        ]
    ]
}
