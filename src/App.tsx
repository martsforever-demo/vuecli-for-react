import React from "react";
import './App.scss'

import {designPage, reactive} from 'plain-design-composition'
import {PlButton} from 'plain-design'

export const App = designPage(() => {
    const state = reactive({
        count: 0
    })

    return () => <>
        <PlButton label={'BUTTON'} onClick={() => state.count++}/>
        <button onClick={() => state.count++}>count: {state.count}</button>
    </>
})